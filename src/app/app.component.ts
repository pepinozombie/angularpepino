import { Component } from '@angular/core';

import { MenuService } from './services/menu.service';

@Component({
  selector: 'application-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
 
export class AppComponent {

  title : string = 'Angular 2 Application';
  menubuilder : MenuService;

  headerItems;
  footerItems;

  constructor(private menuService : MenuService) {
    this.menubuilder = menuService;
  }

  ngOnInit() {
    this.headerItems = this.buildHeaderItems();
  }

  buildHeaderItems() {
    return this.menubuilder.scanDir()
  }
 
}
