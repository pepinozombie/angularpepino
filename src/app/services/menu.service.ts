import { Injectable, Inject } from '@angular/core';

import * as Fs from 'fs';
import * as Path from 'path';

//var Fs = require('fs');
//var Path = require('path');

@Injectable()
export class MenuService {
  
  fileSytem;
  pathSystem;
  configFile : string = 'config.json';
  header : string = 'header';
  footer : string = 'footer';
  
  constructor(@Inject(Fs) fs, @Inject(Path) path) {
    this.fileSytem = fs;
    this.pathSystem = path;
  }

  scanDir() {
    
    let dir = __dirname;
    try {
      this.fileSytem.readdir(dir, function(err, files) {
      if(err) {
        //throw err;
        console.log(err);
      }

      /*files.map(function (file) {
        return this.path.join(dir, file);
      });*/

    });  
    } catch (error) {
      console.log(error);
    }
    

  }

}
